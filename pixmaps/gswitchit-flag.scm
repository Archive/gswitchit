; Copyright Sergey V. Oudaltsov 2003.  

; Written for GSwitchIt GNOME XKB Toolkit

; The very first version was kindly submitted by Alan Horkan

(define (gswitchit-flag-macro image drawable)
; Make this whole thing one big undoable action
        (gimp-undo-push-group-start image)   
; some useful preparation works (resizing and changing the mode)
        (if (= (car (gimp-drawable-is-rgb drawable)) 0)
            (gimp-convert-rgb image))
        (if (not (and (= (car (gimp-image-width image)) 48)
                      (= (car (gimp-image-height image)) 48)))
            (gimp-image-scale image 48 48 0 0))
; 
        (gimp-selection-all image)	
        (gimp-selection-shrink image 1)	
        
        (script-fu-add-bevel image drawable 4 0 0)	
; Mark the end of the group of actions to be undone
        (gimp-undo-push-group-end image)
        (gimp-displays-flush)
        ; TODO: save as XPM ?
)

(script-fu-register "gswitchit-flag-macro"    ; function name
        _"<Image>/Script-Fu/Flag->GSwitchIt Button" 	; put in context menu
        "A macro to help making buttons for Gswitchit."		; description
        "Sergey V. Oudaltsov, svu@users.sourceforge.net"			; author
        "Copyright Sergey V. Oudaltsov 2003"	; copyright
        "January 22, 2003"			; date created
        ""		; image type that the script works on.  
        SF-IMAGE "Input Image" 0
        SF-DRAWABLE "Input Drawable" 0
)
