#!/bin/sh
#
# If you want your screen to blink on layout switching, do the following:
#
# 1. Put blink.sh into any directory in your PATH (for example, $HOME/bin)
#
# 2. Run
#
# $ gconftool-2 -t string -s /apps/gswitchit/Commands/0 "blink.sh 0"
# $ gconftool-2 -t string -s /apps/gswitchit/Commands/1 "blink.sh 1"
#
# These commands add 2 GConf settings to run blink.sh with parameters 0 and 1
# correspondingly.
#
# Use the same way to run any command on the layout switching.
# 
# svu, 19.11.2002
#

XGAMMA='xgamma -q'
DELAY='usleep 50000'

case $1 in
0)
  play /usr/share/sounds/gtk-events/clicked.wav &
  $XGAMMA -ggamma 0.1 -bgamma 0.1
  $DELAY
  $XGAMMA -ggamma 1 -bgamma 1
  ;;
1)
  play /usr/share/sounds/gtk-events/toggled.wav &
  $XGAMMA -ggamma 0.1 -rgamma 0.1
  $DELAY
  $XGAMMA -ggamma 1 -rgamma 1
esac

  
